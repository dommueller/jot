#[macro_use]
extern crate clap;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate text_io;
extern crate toml;
extern crate xdg;

mod cli;
mod config;
#[macro_use]
mod database;
mod errors;

use clap::ArgMatches;
use errors::*;
use std::panic;

macro_rules! disallow_custom {
    ($m:expr) => ({
        if $m.is_present("config") || $m.is_present("database") {
            println!("[!] Config or database paths are not yet supported by flags");
        }
    })}

fn new_jot(matches: &ArgMatches<'static>) -> Result<()> {
    disallow_custom!(matches);

    let content: String = if matches.is_present("msg") {
        matches.value_of("msg").unwrap().to_owned()
    } else {
        println!("New jot: ");
        read!("{}\n")
    };

    let conn = match database::open() {
        Ok(c) => c,
        Err(c) => bail!(c),
    };
    match database::insert_entry(&content, &conn) {
        Ok(_) => close_with_message!("[+] jot added", conn),
        Err(e) => {
            println!("[!] failed to add jot");
            Err(e.into())
        }
    }
}

fn edit_jot(matches: &ArgMatches<'static>) -> Result<()> {
    disallow_custom!(matches);

    let conn = match database::open() {
        Ok(c) => c,
        Err(c) => bail!(c),
    };
    let index = matches
        .value_of("NUMBER")
        .unwrap()
        .parse::<isize>()
        .unwrap();

    let content: String = if matches.is_present("msg") {
        matches.value_of("msg").unwrap().to_owned()
    } else {
        let prev = database::fetch_entry(&index, &conn).unwrap();
        println!("Previous content: {}", prev);
        read!("{}\n")
    };

    match database::update_entry(&index, &content, &conn) {
        Ok(_) => close_with_message!("[+] jot #{} updated", index, conn),
        Err(e) => {
            println!("[!] failed to update jot");
            Err(e.into())
        }
    }
}

fn remove_jot(matches: &ArgMatches<'static>) -> Result<()> {
    disallow_custom!(matches);

    let conn = match database::open() {
        Ok(c) => c,
        Err(c) => bail!(c),
    };
    let index = matches
        .value_of("NUMBER")
        .unwrap()
        .parse::<isize>()
        .unwrap();

    match database::remove_entry(&index, &conn) {
        Ok(_) => close_with_message!("[-] jot #{} removed", index, conn),
        Err(e) => {
            println!("[!] failed to remove jot");
            Err(e.into())
        }
    }
}

fn list_jots(matches: &ArgMatches<'static>) -> Result<()> {
    disallow_custom!(matches);

    let conn = match database::open() {
        Ok(c) => c,
        Err(c) => bail!(c),
    };

    let format = matches.value_of("format");
    let padding = matches.value_of("padding");

    match database::print_table(&conn, format, padding) {
        Ok(_) => close_with_message!(conn),
        Err(e) => Err(e.into()),
    }
}

fn run() -> Result<()> {
    panic::set_hook(Box::new(|info| {
        println!("{}", info.payload().downcast_ref::<String>().unwrap());
    }));

    let matches = cli::cli().get_matches();

    match matches.subcommand() {
        ("add", Some(sub_matches)) => new_jot(sub_matches),
        ("edit", Some(sub_matches)) => edit_jot(sub_matches),
        ("remove", Some(sub_matches)) => remove_jot(sub_matches),
        ("list", Some(sub_matches)) => list_jots(sub_matches),
        _ => unreachable!(),
    }
}

#[cfg(unix)]
quick_main!(run);
